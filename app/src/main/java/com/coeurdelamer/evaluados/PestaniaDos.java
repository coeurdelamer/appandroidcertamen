package com.coeurdelamer.evaluados;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.coeurdelamer.evaluados.R;

public class PestaniaDos extends Fragment {

    EditText etMail, etPhone;
    Button btNextTwo;

    public PestaniaDos() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_pestania_dos,container,false);

        etMail = (EditText)view.findViewById(R.id.etMail);
        etPhone = (EditText)view.findViewById(R.id.etPhone);
        btNextTwo = (Button) view.findViewById(R.id.btNextTwo);

        btNextTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String email = etMail.getText().toString();
                String telefono = etPhone.getText().toString();

                if(!email.isEmpty()){
                    FragmentTransaction fr = getFragmentManager().beginTransaction();
                    fr.replace(R.id.fragment_container, new PestaniaTres());
                    fr.commit();


                }else{
                    Toast.makeText(getActivity(), "El email es obligatorio", Toast.LENGTH_SHORT).show();
                }


            }
        });



        return view;
    }

}

